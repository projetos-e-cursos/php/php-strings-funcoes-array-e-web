# PHP: strings, funcoes, array e web

## Avançando em PHP: Arrays, Strings, Função e Web.

### Conceitos abordados:
- Arrays (listas);
- array() ou [] separados por vírgula ',';
- count(lista);
- Arrays associativos;
- Como acessar valores de arrays associativos;
- foreach;
- indices nao numericos;
- Sequência de indicês;
- Adicionar item na lista sem saber o próximo índice;
- Funções;
- include e require;
- Organizar melhor o código;
- Padrões no PHP;
- mb_strtoupper;
- Remover itens do array (unset);
- PHP na WEB;
- Como iniciar um server local;
- Como apresentar valores na tela;
- Utilizar HTML;
- Utilizar PHP dentro do HTML.
