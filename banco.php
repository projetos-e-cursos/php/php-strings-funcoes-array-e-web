<?php
require 'funcoes.php';

$contasCorrentes = [
    '123.456.789.10' => [
        'nome' => 'Vinicius',
        'saldo' => 1200
    ],
    '123.458.689-11' => [
        'nome' => 'Maria',
        'saldo' => 10000
    ]    ,
    '123.256.789-12' => [
        'nome' => 'Alberto',
        'saldo' => 300
    ],
    '123.278.458-30' => [
        'nome' => 'Erick',
        'saldo' => 15000
    ]
];

$contasCorrentes['123.278.458-30'] = sacar(300, $contasCorrentes['123.278.458-30']);

$contasCorrentes['123.278.458-30'] = depositar(1500, $contasCorrentes['123.278.458-30']);

unset($contasCorrentes['123.456.789.10']);

titularComLetrasMaiusculas($contasCorrentes['123.278.458-30']);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Contas Correntes</h1>

    <dl>
        <?php foreach ($contasCorrentes as $cpf => $conta) { ?>
        <dt>
            <h3><?= $conta['nome']; ?> - <?= $cpf; ?></h3>
        </dt>
        <dd>
            Saldo: <?= $conta['saldo']; ?>
        </dd>
        <?php } ?>
    </dl>
</body>
</html>
