<?php

$contasCorrentes = [
    [
        'nome' => 'Vinicius',
        'saldo' => 1200
    ],
    [
        'nome' => 'Maria',
        'saldo' => 10000
    ]    ,
    [
        'nome' => 'Alberto',
        'saldo' => 300
    ]
];

$contasCorrentes[] = [
    'nome' => 'Erick',
    'saldo' => 15000
];
foreach ($contasCorrentes as $conta) {
    echo $conta['nome'] . PHP_EOL;
}
