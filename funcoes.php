<?php

function exibeMensagem($mensagem)
{
    echo $mensagem . '<br>';
}

function sacar(float $valor, array $conta): array
{
    if($valor > $conta['saldo']) {
        exibeMensagem('Não pode sacar esse valor!');
    } else {
        exibeMensagem('Valor pode ser sacado!');
        $conta['saldo'] -= 500;
        exibeMensagem("Novo saldo da conta: " . $conta["saldo"]);
    }
    return $conta;
}

function depositar(float $valor, array $conta): array {
    if ($valor > 0) {
        $conta['saldo'] += $valor;
        exibeMensagem("Novo saldo da conta: " . $conta["saldo"]);
    } else {
        exibeMensagem("Depositos precisam ser positivos");
    }
    return $conta;
}

function titularComLetrasMaiusculas(array &$conta)
{
    $conta['nome'] = mb_strtoupper($conta['nome']);
}

function exibeConta(array $conta)
{
    ['nome' => $titular, 'saldo' => $saldo] = $conta;
    echo "<li>Titular: $titular. Saldo: $saldo</li>";
}
